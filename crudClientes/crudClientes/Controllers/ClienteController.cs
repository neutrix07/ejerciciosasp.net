﻿using crudClientes.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core_Empleados.Controllers
{
    public class ClienteController : Controller
    {
        private readonly ApplicationDbContext _db;
        public ClienteController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var displaydata = _db.Cliente.ToList();
            return View(displaydata);
        }

//muestra pantalla 
        public IActionResult Crear()
        {
            return View();
        }
//creacion de cliente
        

//detalle de cliente
        public async Task<IActionResult> Detalle(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var getDetalleCliente = await _db.Cliente.FindAsync(id);
            return View(getDetalleCliente);
        }
    
//edicion de cliente
        public async Task<IActionResult> Editar(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var getEmpDetail = await _db.Cliente.FindAsync(id);
            return View(getEmpDetail);
        }
        //este metodo sirve para guardar los datos en el update
        [HttpPost]
        public async Task<IActionResult> Editar(Cliente oldCli)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldCli);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldCli);
        }

        //eliminar cliente
        public async Task<IActionResult> Eliminar(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var empDetail = await _db.Cliente.FindAsync(id);
            return View(empDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Eliminar(int id)
        {

            var getEmpDetail = await _db.Cliente.FindAsync(id);
            _db.Cliente.Remove(getEmpDetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> Index(string busquedaCliente)
        {
            ViewData["GetClienteDetalle"] = busquedaCliente;
            var empquery = from x in _db.Cliente select x;
            if (!String.IsNullOrEmpty(busquedaCliente))
            {
                empquery = empquery.Where(x => x.NombreCliente.Contains(busquedaCliente)
                || x.CorreoCliente.Contains(busquedaCliente));
            }
            return View(await empquery.AsNoTracking().ToListAsync());
        }
    }
}
