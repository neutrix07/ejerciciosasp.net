﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace crudClientes.Models
{
    public class Cliente
    {
        [Key]
        public int IdCliente { get; set; }

        [Required(ErrorMessage = "Ingrese el nombre del cliente!")]
        [Display(Name = "Nombre del cliente")]
        public string NombreCliente { get; set; }

        [Required(ErrorMessage = "Ingrese el credito del cliente")]
        [Display(Name = "Credito :")]
        public float CreditoCliente { get; set; }

        [Required(ErrorMessage = "Ingrese una edad valida")]
        [Display(Name = "Edad :")]
        [Range(20, 50)]
        public int EdadCliente { get; set; }

        [Required(ErrorMessage = "Ingrese un correo valido")]
        [Display(Name = "Correo :")]
        [EmailAddress(ErrorMessage = "Ingrese un correo valido")]
        public string CorreoCliente { get; set; }
    }
}
