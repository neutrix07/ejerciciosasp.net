﻿using Core_Empleados.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



namespace Core_Empleados.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly ApplicationDbContext _db;
        public EmployeeController(ApplicationDbContext db) {
            _db = db;
        }

        public IActionResult Index() {
            var displaydata = _db.Employee.ToList();
            return View(displaydata);
        }
    //muestra pantalla 
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task <IActionResult> Create (Employee nEmp)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nEmp);
        }

        public async Task <IActionResult> Detail(int? id)
        {
            if ( id == null )
            {
                return RedirectToAction("Index");
            }

            var getEmpDetail = await _db.Employee.FindAsync(id);
            return View(getEmpDetail);
        }

        public async Task <IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var getEmpDetail = await _db.Employee.FindAsync(id);
            return View(getEmpDetail);
        }
//este metodo sirve para guardar los datos en el update
        [HttpPost]
        public async Task <IActionResult> Edit (Employee oldEmp)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldEmp);
        }

        public async Task <IActionResult> Delete(int? id)
        {
            if( id == null)
            {
                return RedirectToAction("Index");
            }
            var empDetail = await _db.Employee.FindAsync(id);
            return View(empDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {

            var getEmpDetail = await _db.Employee.FindAsync(id);
            _db.Employee.Remove(getEmpDetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task <IActionResult> Index(string empSearch)
        {
            ViewData["GetEmployeeDetails"] = empSearch;
            var empquery = from x in _db.Employee select x;
            if (!String.IsNullOrEmpty(empSearch))
            {
                empquery = empquery.Where(x => x.Empname.Contains(empSearch)
                || x.Email.Contains(empSearch));
            }
            return View(await empquery.AsNoTracking().ToListAsync());
        }
    }
}
