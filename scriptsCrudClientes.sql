/*Creacion de la Base de Datos*/
CREATE DATABASE clientes_crud;

/*usamos la base de datos*/
USE clientes_crud;

/*creamos la tabla cliente*/
CREATE TABLE Cliente(
	idCliente int not null primary key identity(1,1),
	nombreCliente nvarchar(100),
	creditoCliente float(11),
	edadCliente int,
	correoCliente nvarchar(30)
);
/*insertamos 2 registtros*/
INSERT INTO cliente values('Oscar Mendoza', 10000, 26, 'oscar_mdp10@gmail.com');
INSERT INTO cliente values('Luis CG', 11000, 24, 'luiscg@gmail.com');

