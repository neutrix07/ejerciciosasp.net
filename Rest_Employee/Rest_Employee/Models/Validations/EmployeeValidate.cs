﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Rest_Employee.Models
{
    public partial class Employee
    {

        [MetadataType(typeof(Employee.MetaData))]
        sealed class MetaData
        {
            [Key]
            public int Empid;
            
            [Required(ErrorMessage = "Ingrese un nombre valido para el empleado")]
            public string Empname;
            [Required]
            [EmailAddress(ErrorMessage = "Ingrese un emaill valido!")]
            public string Email;

            public Nullable<int> Age;
            
            public Nullable<int> Salary;
        }
    }
}